const path = require('path');
const { paths } = require('react-app-rewired');

const rewireStyledComponents = require('react-app-rewire-styled-components');
const rewireAliases = require('react-app-rewire-aliases');
const ThreeExamples = require('import-three-examples');

module.exports = (config, env) => {
  config = rewireAliases.aliasesOptions({
    '@src': path.resolve(__dirname, paths.appSrc)
  })(config, env);

  config = rewireStyledComponents(config, env);

  config.module.rules = [...config.module.rules, ...ThreeExamples];

  return config;
};
