import React, { Component } from 'react';

import '@src/App.css';
 import HomeView from '@src/views/HomeView';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HomeView />
      </div>
    );
  }
}

export default App;
