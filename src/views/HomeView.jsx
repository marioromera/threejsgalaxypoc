import React from 'react';
import PropTypes from 'prop-types';
import Orbit3d from '../components/Orbit3d';

const propTypes = {};

const defaultProps = {};

export default class HomeView extends React.Component {
    render() {
        return (
            <React.Fragment>
    <Orbit3d/>
            </React.Fragment>
        );
    }
}

 HomeView.propTypes = propTypes;
 HomeView.defaultProps = defaultProps;