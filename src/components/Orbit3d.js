import React, { useRef, useState, useEffect } from 'react';
import * as THREE from 'three';
import * as TWEEN from '@tweenjs/tween.js';
import OrbitControls from 'three/examples/js/controls/OrbitControls';
import CANNON from 'cannon';
import {
  EffectComposer,
  ShaderPass,
  BloomPass,
  DotScreenPass,
  RenderPass,
  CopyShader,
  MaskPass,
  ClearMaskPass,
  ClearPass
} from 'three-full';
// var EquirectangularToCubemap = require('three.equirectangular-to-cubemap');

const Orbit3d = () => {
  const mount = useRef(null);
  const [isAnimating, setAnimating] = useState(true);
  const animationControls = useRef(null);

  useEffect(() => {
    let frameId;
    let selectableObjects = [];
    let width = window.clientWidth;
    let height = window.clientHeight;
    let composer;
    let world;
    let springs = [];
    let planets = [];
    let orbitPlanets = [];
    let attractBodies = [];
    let cannonPlanets = [];
    let tweens = [];
    let clickMarker;
    let plane = false;
    let pivot;
    let mouseConstraint = false;
    let jointBody, constrainedBody;
    let raycaster = new THREE.Raycaster();
    const galaxyScene = new THREE.Scene();
    const planetsScene = new THREE.Scene();

    const camera = new THREE.PerspectiveCamera(60, 4 / 3, 0.1, 10000.0);
    camera.position.set(50.0, 50.0, 50.0);
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.autoClear = false;

    renderer.setSize(width, height);

    var geometry = new THREE.SphereGeometry(590, 160, 140);
    const getTextureMaterial = textureJpg => {
      var uniforms = {
        texture: {
          type: 't',
          value: new THREE.TextureLoader().load(textureJpg)
        }
      };

      var textureMaterial = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: `
      varying vec2 vUV; 
      void main() {
        vUV = uv;
        vec4 pos = vec4(position, 1.0);
        gl_Position = projectionMatrix * modelViewMatrix * pos;
      }`,
        fragmentShader: `
      uniform sampler2D texture;
      varying vec2 vUV; 
      void main() { 
        vec4 sample = texture2D(texture, vUV);
        gl_FragColor = vec4(sample.xyz, sample.w);
      }`
      });
      return textureMaterial;
    };

    const skyBox = new THREE.Mesh(
      geometry,
      getTextureMaterial('/assets/milky-way-1655504_1920.jpg')
    );
    skyBox.material.side = THREE.BackSide;

    galaxyScene.add(skyBox);

    const ambientLight = new THREE.AmbientLight(0xffffff, 1.1);
    const ambientLight1 = new THREE.AmbientLight(0xffffff, 0.1);
    galaxyScene.add(ambientLight);
    planetsScene.add(ambientLight1);

    const controls = new OrbitControls(camera, renderer.domElement);
    controls.autoRotate = false;
    controls.maxDistance = 90000.0;
    controls.maxZoom = 1.0;
    controls.rotateSpeed = 0.4;
    controls.panSpeed = 0.001;
    controls.enableDamping = true;
    controls.dampingFactor = 1; // friction

    galaxyScene.add(controls);

    const init = () => {
      world = new CANNON.World();
      world.quatNormalizeSkip = 0;
      world.quatNormalizeFast = false;
      world.gravity.set(0, 0, 0);
      world.broadphase = new CANNON.NaiveBroadphase();
      world.defaultContactMaterial.contactEquationStiffness = 1e7;
      world.defaultContactMaterial.contactEquationRelaxation = 4;
      // Joint body
      var shape = new CANNON.Sphere(0.001);
      jointBody = new CANNON.Body({ mass: 0 });
      jointBody.addShape(shape);
      jointBody.collisionFilterGroup = 0;
      jointBody.collisionFilterMask = 0;
      world.addBody(jointBody);

      const planeMaterial = new THREE.MeshBasicMaterial({
        color: 0xff5800
      });
      planeMaterial.visible = false;
      plane = new THREE.Mesh(new THREE.PlaneGeometry(500, 500, 8, 8), planeMaterial);
      planetsScene.add(plane);

      // Create a plane
      var groundShape = new CANNON.Plane();
      var groundBody = new CANNON.Body({ mass: 0, wireframe: true });
      groundBody.addShape(groundShape);
      groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);

      world.addBody(groundBody);

      const solar = new THREE.Mesh(
        new THREE.SphereGeometry(1.391016, 32, 32),
        new THREE.MeshPhongMaterial({ emissive: 0xff5800, emissiveIntensity: 0.5 })
      );

      let pointLight = new THREE.PointLight('#ccffcc', 2.0, 300.0);
      solar.add(pointLight);
      planetsScene.add(solar);

      // +00,000km          hours
      //       name    diameter  distance  tilt   dayLength     speed
      createPlanet('Mercury', 0.4879, 5.79, 0.7, 4222.6, 88);
      createPlanet('Venus', 1.2104, 10.82, 3.4, 2802, 224);
      createPlanet('Earth', 1.2756, 14.96, 0, 24, 365);
      createPlanet('Mars', 0.6792, 22.79, 1.85, 24.7, 687);
      createPlanet('Jupiter', 14.2984, 77.86, 1.303, 9.9, 4331);
      createPlanet('Saturn', 12.0536, 143.35, 2.48524, 10.7, 10747);
      createPlanet('Uranus', 5.1118, 287.25, 0.8, 17.2, 30589);
      createPlanet('Neptune', 4.9528, 449.51, 1.8, 16.1, 59800);
      createPlanet('Pluto', 0.2374, 587.4, 17.2, 6.39, 90570);

      composer = new EffectComposer(renderer);

      var ratio = 1;
      composer.setSize(window.innerWidth * ratio, window.innerHeight * ratio);
      composer.renderTarget1.stencilBuffer = true;
      composer.renderTarget2.stencilBuffer = true;

      const galaxyRenderPass = new RenderPass(galaxyScene, camera);
      galaxyRenderPass.clear = false;
      const planetsRenderPass = new RenderPass(planetsScene, camera);
      planetsRenderPass.clear = false;

      const inversePlanetMaskPass = new MaskPass(planetsScene, camera);
      inversePlanetMaskPass.inverse = true;
      const galaxyMaskPass = new MaskPass(galaxyScene, camera);
      galaxyMaskPass.inverse = false;
      const clearMaskPass = new ClearMaskPass();

      const outputPass = new ShaderPass(CopyShader);
      outputPass.renderToScreen = true;

      const dotScreenPass = new DotScreenPass();
      const bloomPass = new BloomPass(3, 25, 5, 256);
      const clearPass = new ClearPass();

      composer.addPass(clearPass);
      composer.addPass(galaxyRenderPass);
      composer.addPass(planetsRenderPass);
      composer.addPass(inversePlanetMaskPass);
      composer.addPass(dotScreenPass);
      composer.addPass(clearMaskPass);
      composer.addPass(bloomPass);
      composer.addPass(outputPass);

      renderer.domElement.addEventListener('mousemove', onMouseMove, false);
      renderer.domElement.addEventListener('mousedown', onMouseDown, false);
      renderer.domElement.addEventListener('mouseup', onMouseUp, false);

      world.addEventListener('postStep', function(event) {
        for (var i = 0; i !== springs.length; i++) {
          if (!constrainedBody || (constrainedBody && constrainedBody.index !== i)) {
            springs[i].applyForce();
          }
        }
      });
    };

    const createPlanet = (name, radius, distance, tilt, dayLength, speed) => {
      let color = 'white';
      let orbitContainer = new THREE.Object3D();
      orbitContainer.rotation.x = tilt;

      let orbit = new THREE.Object3D();

      let geometry = new THREE.CircleGeometry(distance, 100);
      geometry.vertices.shift();
      let line = new THREE.LineLoop(
        geometry,
        new THREE.LineBasicMaterial({ color: 'aqua', visible: false })
      );
      let planet = new THREE.Mesh(
        new THREE.SphereGeometry(radius, 32, 32),
        getTextureMaterial(`/assets/${name.toLowerCase()}.jpg`)
      );
      planet.rotation.y += Math.PI * dayLength;

      planet.position.set(distance, 0, 0);
      let cannonPlanet = new CANNON.Body({
        mass: 1,
        shape: new CANNON.Sphere(0.001)
      });

      planet.name = name;

      cannonPlanet.position.set(distance, 0, 0);

      world.add(cannonPlanet);
      cannonPlanets.push(cannonPlanet);

      // Create a static sphere
      var sphereShape = new CANNON.Sphere(0.1);
      const attractBody = new CANNON.Body({ mass: 0 });
      attractBody.addShape(sphereShape);

      world.add(attractBody);
      attractBodies.push(attractBody);

      const spring = new CANNON.Spring(cannonPlanet, attractBody, {
        localAnchorA: new CANNON.Vec3(0, 0, 0),
        localAnchorB: new CANNON.Vec3(-0.001, 0, 0),
        restLength: 0,
        stiffness: 10,
        damping: 5
      });
      springs.push(spring);
      planets.push(planet);
      selectableObjects.push(planet);
      const orbitPlanetMaterial = new THREE.MeshPhongMaterial({ color: color });
      orbitPlanetMaterial.visible = false;
      let orbitPlanet = new THREE.Mesh(
        new THREE.SphereGeometry(radius, 32, 32),
        orbitPlanetMaterial
      );
      orbitPlanet.position.set(distance, 0, 0);
      orbitPlanet.visible = true;
      orbitPlanets.push(orbitPlanet);
      orbit.add(line);
      orbit.add(orbitPlanet);
      orbit.add(planet);

      const tween2 = new TWEEN.Tween(planet.rotation).to({ y: '+' + Math.PI * 2 }, dayLength * 10);
      tween2.onComplete(function() {
        tween2.start();
      });
      tween2.start().repeat(Infinity);
      const tween = new TWEEN.Tween(orbit.rotation).to({ y: '+' + Math.PI * 2 }, speed * 100);
      tween.onComplete(function() {
        tween.start();
      });
      tween.start().repeat(Infinity);
      tweens.push(tween);
      orbitContainer.add(orbit);

      planetsScene.add(orbitContainer);

      return orbitContainer;
    };
    let entity;
    const onMouseDown = e => {
      // Find mesh from a ray
      entity = findNearestIntersectingObject(e.clientX, e.clientY, camera, planets);

      var pos = entity.point;
      if (pos && entity.object.geometry instanceof THREE.SphereGeometry) {
        // Set the movement plane
        setScreenPerpCenter(pos, camera);

        var idx = planets.indexOf(entity.object);
        if (idx !== -1) {
          addMouseConstraint(pos.x, pos.y, pos.z, attractBodies[idx], idx);
        }
      }
    };

    const onMouseMove = e => {
      if (plane && mouseConstraint) {
        var pos = projectOntoPlane(e.clientX, e.clientY, plane, camera);
        if (pos) {
          setClickMarker(pos.x, pos.y, pos.z, planetsScene);
          moveJointToPoint(pos.x, pos.y, pos.z);
        }
      }
    };

    const setScreenPerpCenter = (point, camera) => {
      if (!plane) {
        var planeGeo = new THREE.PlaneGeometry(100, 100);
        const planeMaterial = new THREE.MeshPhongMaterial({ color: 0xff0000 });
        plane = new THREE.Mesh(planeGeo, planeMaterial);
        plane.visible = false; // Hide it..
        planetsScene.add(plane);
      }
      // Center at mouse position
      plane.position.copy(point);

      // Make it face toward the camera
      plane.quaternion.copy(camera.quaternion);
    };

    let lastx, lasty, last;
    const projectOntoPlane = (screenX, screenY, thePlane, camera) => {
      var x = (screenX / window.innerWidth) * 2 - 1;
      var y = -(screenY / window.innerHeight) * 2 + 1;
      var now = new Date().getTime();
      // project mouse to that plane
      var hit = findNearestIntersectingObject(screenX, screenY, camera, [thePlane]);
      lastx = x;
      lasty = y;

      last = now;
      if (hit) return hit.point;
      return false;
    };

    const findNearestIntersectingObject = (clientX, clientY, camera, objects) => {
      var x = (clientX / window.innerWidth) * 2 - 1;
      var y = -(clientY / window.innerHeight) * 2 + 1;

      // Get the picking ray from the point
      raycaster.setFromCamera({ x, y }, camera);
      // Find the closest intersecting object
      // Now, cast the ray all render objects in the scene to see if they collide. Take the closest one.
      var hits = raycaster.intersectObjects(objects);
      var closest = false;
      if (hits.length > 0) {
        closest = hits[0];
      }
      return closest;
    };

    const addMouseConstraint = (x, y, z, body, idx) => {
      controls.enabled = false;

      tweens[idx].stop();

      // The cannon body constrained by the mouse joint
      constrainedBody = body;
      constrainedBody.index = idx;

      // Vector to the clicked point, relative to the body
      var v1 = new CANNON.Vec3(x, y, z).vsub(constrainedBody.position);
      // Apply anti-quaternion to vector to tranform it into the local body coordinate system
      var antiRot = constrainedBody.quaternion.inverse();
      pivot = antiRot.vmult(v1); // pivot is not in local body coordinates

      // Move the cannon click marker particle to the click position
      jointBody.position.set(x, y, z);
      // Create a new constraint
      // The pivot for the jointBody is zero
      mouseConstraint = new CANNON.PointToPointConstraint(
        constrainedBody,
        pivot,
        jointBody,
        new CANNON.Vec3(0, 0, 0)
      );
      // Add the constriant to world
      world.addConstraint(mouseConstraint);
    };

    const moveJointToPoint = (x, y, z) => {
      // Move the joint body to a new position

      jointBody.position.set(x, y, z);
      mouseConstraint.update();
    };

    const removeJointConstraint = () => {
      // Remove constriant from world
      world.removeConstraint(mouseConstraint);
      mouseConstraint = false;
      constrainedBody = false;
      controls.enabled = true;
      for (var i = 0; i !== tweens.length; i++) {
        tweens[i].start();
      }
    };

    const setClickMarker = (x, y, z) => {
      if (!clickMarker) {
        const markerMaterial = new THREE.MeshLambertMaterial({ color: 0xff0000 });
        markerMaterial.visible = false;
        var shape = new THREE.SphereGeometry(0.2, 8, 8);
        clickMarker = new THREE.Mesh(shape, markerMaterial);
        planetsScene.add(clickMarker);
      }
      clickMarker.name = 'clickMarker';
      clickMarker.visible = true;
      clickMarker.position.set(x, y, z);
    };

    const onMouseUp = event => {
      // remove the marker
      removeClickMarker();
      // Enable the controls
      removeJointConstraint();
    };

    const removeClickMarker = () => {
      if (clickMarker) clickMarker.visible = false;
    };

    const handleResize = () => {
      width = window.innerWidth;
      height = window.innerHeight;

      camera.aspect = width / height;
      camera.updateProjectionMatrix();
      renderer.setSize(width, height);
      renderer.gammaInput = true;
      renderer.gammaOutput = true;
      renderer.shadowMap.enabled = true;
    };

    const animate = time => {
      skyBox.rotation.y += 0.0001;
      frameId = window.requestAnimationFrame(animate);
      updatePhysics(1 / 60);
      TWEEN.update(time);
      controls.update();
      composer.render();
    };

    const updatePhysics = dt => {
      world.step(dt);

      for (var i = 0; i !== planets.length; i++) {
        if (!constrainedBody || (constrainedBody && constrainedBody.index !== i)) {
          springs[i].stiffness = 10;
          springs[i].bodyB = attractBodies[i];
          attractBodies[i].position.copy(orbitPlanets[i].position);
          attractBodies[i].quaternion.copy(orbitPlanets[i].quaternion);
        } else {
          springs[i].bodyB = jointBody;
          springs[i].stiffness = 50;
          springs[i].damping = 5;
          springs[i].applyForce();
        }
        planets[i].position.copy(cannonPlanets[i].position);
        planets[i].quaternion.copy(cannonPlanets[i].quaternion);
      }
    };

    mount.current.appendChild(renderer.domElement);
    window.addEventListener('resize', handleResize);
    handleResize();
    init();

    const start = () => {
      if (!frameId) {
        frameId = requestAnimationFrame(animate);
      }
    };

    const stop = () => {
      cancelAnimationFrame(frameId);
      frameId = null;
    };
    animationControls.current = { start, stop };

    return () => {
      stop();
      cancelAnimationFrame();
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    if (isAnimating) {
      animationControls.current.start();
    } else {
      animationControls.current.stop();
    }
  }, [isAnimating]);
  /* eslint-disable
    jsx-a11y/click-events-have-key-events,
    jsx-a11y/no-static-element-interactions
  */
  return <div className="vis" ref={mount} />;
};

export default Orbit3d;
